export default {
    computed:{
        convertItems(){
            let converted = (this.selectedMoreName.length > 0 && this.needShowAll === true)
                ? [...this.items, {
                    needShowAll: 'true',
                    title: `Show all ${this.selectedMoreName} radiocasts`,
                }]
                : this.items;
            return converted;
        },
        themeClass() {
            return {
                cardClass: this.theme == 'white'
                    ? 'bg-white'
                    : 'bg-primary',
                swgName: this.theme == 'white'
                    ? 'white'
                    : 'black',
            };
        },
    }
}