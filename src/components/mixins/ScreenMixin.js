export default {
    computed:{
        isMobile(){
            const mobileScreenSizes = [ 'xs', 'sm', 'md' ];
            return (mobileScreenSizes.indexOf(this.$store.state.screenSize) != -1);
        },
        isDesktop(){
            const mobileScreenSizes = [ 'xl', 'lg' ];
            return (mobileScreenSizes.indexOf(this.$store.state.screenSize) != -1);
        },
    }
}