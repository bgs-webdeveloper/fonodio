import axios from "axios";
import { API_URL, TAGS_LOVES, TAGS_HATES } from "@/helpers/constants";

export const GET_TEST = () => axios.get(`https://jsonplaceholder.typicode.com/users/1/posts`);

// Auth
export const POST_REGISTER = (payload) => axios.post(`${API_URL}/register`, payload);
export const POST_LOGIN = (payload) => axios.post(`${API_URL}/oauth/`, payload);

// Favorites
export const GET_LOVES_TAGS = () => axios.get(`${API_URL}/${TAGS_LOVES}`);
export const GET_HATES_TAGS = () => axios.get(`${API_URL}/${TAGS_HATES}`);
export const POST_LOVES_TAGS = (tags_ids) => axios.post(`${API_URL}/${TAGS_LOVES}`, { tags_ids });
export const POST_HATES_TAGS = (tags_ids) => axios.post(`${API_URL}/${TAGS_HATES}`, { tags_ids });
export const DELETE_LOVES_TAGS = (tags_ids) => axios.delete(`${API_URL}/${TAGS_LOVES}`, { tags_ids });
export const DELETE_HATES_TAGS = (tags_ids) => axios.delete(`${API_URL}/${TAGS_HATES}`, { tags_ids });



export const FETCH_HOME_PAGE = () => axios.get(`${API_URL}/`);
export const FETCH_RADIOROOM_PAGE = () => axios.get(`${API_URL}/`);
export const FETCH_RADIOCAST_PAGE = () => axios.get(`${API_URL}/`);
export const FETCH_TOPICROOM = () => axios.get(`${API_URL}/`);
export const FETCH_SEARCH_PAGE = () => axios.get(`${API_URL}/`);