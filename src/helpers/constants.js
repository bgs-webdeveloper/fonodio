export const API_URL = 'https://api.fonodio.radio/v1'

export const TAGS_LOVES = '/user/tags/loves'
export const TAGS_HATES = '/user/tags/hates'