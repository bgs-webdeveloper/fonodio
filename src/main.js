import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required, email, min } from 'vee-validate/dist/rules';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faHeart, faSearch, faMinusCircle, faTimes, faBars, faTimesCircle, faSearchDollar} from '@fortawesome/free-solid-svg-icons';
// import { faPlayCircle } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faFacebookF, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';

library.add(faHeart, faSearch, faMinusCircle, faFacebookF, faInstagram, faTwitter, faTimes, faBars, faTimesCircle, faSearchDollar);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

// No message specified.
extend('email', email);
extend('min', {
  ...min,
  message: 'Pass is short'
});

// Override the default message.
extend('required', {
  ...required,
  message: 'This field is required'
});

// Register it globally
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

import { BIcon, BIconSearch, BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.component('BIcon', BIcon)
Vue.component('BIconSearch', BIconSearch)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import '@/assets/styles/main.scss'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
