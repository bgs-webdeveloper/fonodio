export default [
    {
      path: "/start",
      name: "start",
      component: () =>
        import(/* webpackChunkName: "Start" */ "@/views/Authorization/Start.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "Login" */ "@/views/Authorization/Login.vue")
    },
    {
      path: "/register",
      name: "register",
      component: () =>
        import(/* webpackChunkName: "Register" */ "@/views/Authorization/Register.vue")
    }
]