import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

import auth from "./auth";
import account from "./account";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/search",
    name: "search",
    component: () =>
      import(/* webpackChunkName: "Search" */ "@/views/Search.vue")
  },
  {
    path: "/signed-in",
    name: "signed-in",
    component: () =>
      import(/* webpackChunkName: "SignedIn" */ "@/views/SignedIn.vue")
  },
  {
    path: "/radioroom",
    name: "radioroom",
    component: () =>
      import(/* webpackChunkName: "Radioroom" */ "@/views/Radioroom.vue"),
    meta: { needHeaderCategories: true, fixed: false}
  },
  {
    path: "/topicroom",
    name: "topicroom",
    component: () =>
      import(/* webpackChunkName: "Topicroom" */ "@/views/Topicroom.vue"),
      meta: { needHeaderCategories: true, fixed: false}
  },
  {
    path: "/radiocast",
    name: "radiocast",
    component: () =>
      import(/* webpackChunkName: "Radiocast" */ "@/views/Radiocast.vue"),
    meta: { needHeaderCategories: true, fixed: false}
  },
  {
    path: "/category/:name",
    name: "category",
    component: () =>
        import(/* webpackChunkName: "Radiocast" */ "@/views/Category.vue"),
    meta: { needHeaderCategories: true, fixed: false}
  },
  ...auth,
  ...account
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
