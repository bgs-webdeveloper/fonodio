export default {
    lovesTags(state) {
        return state.lovesTags.map(item => ({
            id: item.id,
            name: item.title.split(" ")[0],
        }))
    }
}