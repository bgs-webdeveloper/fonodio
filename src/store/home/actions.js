import {
    FETCH_HOME_PAGE
} from "@/helpers/allRequests"

export default {
    FETCH_DATA() {
        // context, payload
        FETCH_HOME_PAGE()
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            })
    }
}