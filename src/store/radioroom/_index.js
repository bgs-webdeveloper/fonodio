import actions from './actions'
import getters from './getters'
import { secCategories } from "../../staticData/sections";

export default {
    namespaced: true,
    state: {
        secCategories : secCategories,
        selectedCategory: undefined,
        radiorooms: []
    },
    actions,
    getters
}